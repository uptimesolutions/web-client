/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class DomJsonParser implements Serializable {
    public transient final String DATE_TIME_OFFSET_FORMAT;
    public transient JsonObject jsonObject;
    public transient JsonElement element;
    public transient JsonArray array;

    /**
     * Constructor
     */
    public DomJsonParser() {
        DATE_TIME_OFFSET_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    }
    
    /**
     * Parses Massage Json and returns a String object
     * Contains "outcome" as an element
     * @param inputJson, String object
     * @return String object
     */
    public String getMessageFromJSON(String inputJson) {
        if ((element = JsonParser.parseString(inputJson)) != null) {
            if ((jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("outcome")) {
                    Logger.getLogger(DomJsonParser.class.getName()).log(Level.INFO, "outcome:{0}", jsonObject.get("outcome").getAsString());
                    return jsonObject.get("outcome").getAsString();
                }
            } else 
                Logger.getLogger(DomJsonParser.class.getName()).log(Level.INFO,"No valid JSON response found");
        }
        return "";
    }
}
