/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class ServiceHostVO implements Serializable{
    private String ip;
    private int port;
    private boolean circuitBreaker;
    
    public ServiceHostVO() {
        port = -1;
        circuitBreaker = false;
    }

    public ServiceHostVO(ServiceHostVO serviceHostVO) {
        this.ip = serviceHostVO.getIp();
        this.port = serviceHostVO.getPort();
        this.circuitBreaker = serviceHostVO.isCircuitBreaker();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isCircuitBreaker() {
        return circuitBreaker;
    }

    public void setCircuitBreaker(boolean circuitBreaker) {
        this.circuitBreaker = circuitBreaker;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.ip);
        hash = 79 * hash + this.port;
        hash = 79 * hash + (this.circuitBreaker ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceHostVO other = (ServiceHostVO) obj;
        if (this.port != other.port) {
            return false;
        }
        if (this.circuitBreaker != other.circuitBreaker) {
            return false;
        }
        if (!Objects.equals(this.ip, other.ip)) {
            return false;
        }
        return true;
    }
}
