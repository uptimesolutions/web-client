/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class HttpResponseVO implements Serializable {
    private int statusCode;
    private String entity;
    private boolean hostFound;

    public HttpResponseVO() {
    }

    public HttpResponseVO(HttpResponseVO httpResponseVO) {
        statusCode = httpResponseVO.getStatusCode();
        entity = httpResponseVO.getEntity();
        hostFound = httpResponseVO.isHostFound();
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public boolean isHostFound() {
        return hostFound;
    }

    public void setHostFound(boolean hostFound) {
        this.hostFound = hostFound;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + this.statusCode;
        hash = 61 * hash + Objects.hashCode(this.entity);
        hash = 61 * hash + (this.hostFound ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HttpResponseVO other = (HttpResponseVO) obj;
        if (this.statusCode != other.statusCode) {
            return false;
        }
        if (this.hostFound != other.hostFound) {
            return false;
        }
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        return true;
    }
}
