/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 *
 * @author twilcox
 */
public class JsonConverterUtil {  
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATE_MONTH_FORMATTER = DateTimeFormatter.ofPattern("E, MMM dd, YYYY, HH:mm:ss");
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final String DATE_TIME_OFFSET_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final DateTimeFormatter DATE_TIME_OFFSET_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    
    /**
     * Private class used to convert a LocalDate json
     */
    private static class LocalDateJsonConverter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {
        @Override
        public JsonElement serialize(LocalDate localDate, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(localDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
        }

        @Override
        public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDate.parse(json.getAsString());
        }
    }
    
    /**
     * Private class used to convert a LocalDateTime json
     */
    private static class LocalDateTimeJsonConverter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
        @Override
        public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(localDateTime.format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
        }

        @Override
        public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
        }
    }
    
    /**
     * Private class used to convert an Instant json
     */
    private static class InstantJsonConverter implements JsonSerializer<Instant>, JsonDeserializer<Instant> {
        private final ZoneId ZONE_ID;
        
        /**
         * Constructor
         */
        public InstantJsonConverter() {
            ZONE_ID = ZoneOffset.UTC;
        }
        
        /**
         * Param Constructor
         * @param zoneId, ZoneId Object 
         */
        public InstantJsonConverter(ZoneId zoneId) {
            ZONE_ID = zoneId;
        }

        @Override
        public JsonElement serialize(Instant instant, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT).withZone(ZONE_ID).format(instant));
        }

        @Override
        public Instant deserialize(JsonElement json, Type type, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZONE_ID).toInstant();
        }
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object) throws Exception {
        GsonBuilder builder;
        
        if(object == null) {
            throw new NullPointerException("The given param is null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter());
        
        return builder.disableHtmlEscaping().create().toJson(object);
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object
     * @param zoneId, ZoneId Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, ZoneId zoneId) throws Exception {
        GsonBuilder builder;
        
        if(object == null || zoneId == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter(zoneId));
        
        return builder.disableHtmlEscaping().create().toJson(object);
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object, the main Object to be converted
     * @param property, String Object, used to add a new property to the json
     * @param value, Object, the value set to the new property, this param can be null.
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, String property, Object value) throws Exception {
        JsonElement element;
        GsonBuilder builder;
        Gson gson;
        
        if(object == null || property == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter());
        
        gson = builder.create();
        element = gson.toJsonTree(object);
        element.getAsJsonObject().add(property,gson.toJsonTree(value));
        return gson.toJson(element);
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object, the main Object to be converted
     * @param property, String Object, used to add a new property to the json
     * @param value, Object, the value set to the new property, this param can be null.
     * @param zoneId, ZoneId Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, String property, Object value, ZoneId zoneId) throws Exception {
        JsonElement element;
        GsonBuilder builder;
        Gson gson;
        
        if(object == null || property == null || zoneId == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter(zoneId));
        
        gson = builder.create();
        element = gson.toJsonTree(object);
        element.getAsJsonObject().add(property,gson.toJsonTree(value));
        return gson.toJson(element);
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object, the main Object to be converted
     * @param propertyValueMap, Map Object, with String Objects as the keys and Objects as the values, used to add properties and values to the json
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, Map<String, Object> propertyValueMap) throws Exception {
        JsonElement element;
        GsonBuilder builder;
        Gson gson;
        
        if(object == null || propertyValueMap == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter());
        
        gson = builder.create();
        element = gson.toJsonTree(object);
        propertyValueMap.keySet().forEach(key -> element.getAsJsonObject().add(key,gson.toJsonTree(propertyValueMap.get(key))));
        return gson.toJson(element);
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object, the main Object to be converted
     * @param propertyValueMap, Map Object, with String Objects as the keys and Objects as the values, used to add properties and values to the json
     * @param zoneId, ZoneId Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, Map<String, Object> propertyValueMap, ZoneId zoneId) throws Exception {
        JsonElement element;
        GsonBuilder builder;
        Gson gson;
        
        if(object == null || propertyValueMap == null || zoneId == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter(zoneId));
        
        gson = builder.create();
        element = gson.toJsonTree(object);
        propertyValueMap.keySet().forEach(key -> element.getAsJsonObject().add(key,gson.toJsonTree(propertyValueMap.get(key))));
        return gson.toJson(element);
    }
}
