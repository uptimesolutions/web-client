/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.utils;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.stream.JsonParser;

/**
 *
 * @author twilcox
 */
public class SaxJsonParser {
    public transient final String DATE_TIME_OFFSET_FORMAT;
    public transient JsonParser parser;
    public transient JsonParser.Event event;

    public SaxJsonParser() {
        DATE_TIME_OFFSET_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    }
    
     /**
     * Parses Massage Json and returns a String object
     * Contains "outcome" as an element
     * @param inputJson, String object
     * @return String object
     */
    public String getMessageFromJSON(String inputJson) {
        String message;
        String keyName;
         
        try {
            parser = Json.createParser(new StringReader(inputJson));
            keyName = null;
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case KEY_NAME:
                        if (parser.getString().equalsIgnoreCase("outcome")) {
                            keyName = "outcome";
                        }
                        break;
                    case VALUE_STRING:
                        if(keyName != null && keyName.equalsIgnoreCase("outcome")) {
                            message = parser.getString();
                            return message;
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }

        return "";
    }
    
}
