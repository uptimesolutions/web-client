/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.thread;

import com.uptime.client.http.client.ServiceRegistrarClient;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class ServiceQueryThread implements Runnable {
    private final AtomicBoolean running, stop, dispose;
    private final AtomicLong lastProcessingTime;
    private final Long MAX_PROCESSING_TIME;
    private final int SLEEP_TIME; 
    private final String SERVICE;
    private Thread worker;
    
    /**
     * Constructor
     * @param service, String object
     */
    public ServiceQueryThread(String service) {
        stop = new AtomicBoolean(true);
        running = new AtomicBoolean(false);
        dispose = new AtomicBoolean(false); 
        lastProcessingTime = new AtomicLong(System.currentTimeMillis());
        MAX_PROCESSING_TIME = 600000L; // 10 min
        SLEEP_TIME = 300000; // 5 min
        SERVICE = service;
    }
    
    /**
     * Starts the worker thread
     */
    public void start() {
        stop.set(false);
        worker = new Thread(this);
        worker.start();
    }
    
    /**
     * interrupts the worker thread
     */
    public void interrupt() {
        running.set(false);
        worker.interrupt();
    }
    
    /**
     * @return the stop
     */
    public boolean isStopped() {
        return stop.get();
    }
    
    /**
     * @return the dispose
     */
    public boolean isDispose() {
        return dispose.get();
    }
    
    /**
     * Checks if the worker thread is hung up
     * @return boolean, true is hung up, false otherwise
     */
    public boolean isHung() {
        return (System.currentTimeMillis() - lastProcessingTime.get() > MAX_PROCESSING_TIME);
    }
    
    /**
     * @return the SERVICE
     */
    public String getService() {
        return SERVICE;
    }
    
    /**
     * Runs worker thread.
     * Testing given host
     */
    @Override
    public void run() {
        int count;
        
        running.set(true);
        while(running.get() && !dispose.get()) {
            try{
                lastProcessingTime.set(System.currentTimeMillis());
                
                count = 0;
                while(running.get() && !dispose.get() && count < 5) {
                    try{
                        if(ServiceRegistrarClient.serviceHostsMap == null || !ServiceRegistrarClient.serviceHostsMap.containsKey(SERVICE) || 
                                ServiceRegistrarClient.serviceHostsMap.get(SERVICE) == null || ServiceRegistrarClient.serviceHostsMap.get(SERVICE).isEmpty()) {
                            ServiceRegistrarClient.getInstance().query(SERVICE);
                        } else {
                            dispose.set(true);
                        }
                        
                        // Sleep
                        if(running.get() && !dispose.get()){
                            count++;
                            Thread.sleep(5000);
                        }
                    } catch(InterruptedException e){
                        throw e;
                    } catch(Exception e){
                        throw e;
                    }
                }
                
                // Sleep
                if(running.get() && !dispose.get()){
                    count++;
                    Thread.sleep(SLEEP_TIME);
                }
            } catch(InterruptedException e){
            } catch(Exception e){
                Logger.getLogger(ServiceQueryThread.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        stop.set(true);
    }
}
