/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.thread;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.client.ServiceRegistrarClient.serviceHostsMap;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class SubscriptionMonitorThread implements Runnable {
    private final List<ServiceQueryThread> serviceQueryThreadList;
    private final AtomicLong lastProcessingTime;
    private final Long MAX_PROCESSING_TIME;
    private final AtomicBoolean running, stop;
    private final int SLEEP_TIME; 
    private final String[] SERVICES;
    private boolean testing;
    private Thread worker;
    
    /**
     * Constructor
     * @param services, Array Object of String Objects
     */
    public SubscriptionMonitorThread(String[] services) {
        SERVICES = services;
        stop = new AtomicBoolean(true);
        running = new AtomicBoolean(false);
        serviceQueryThreadList = new ArrayList();
        lastProcessingTime = new AtomicLong(System.currentTimeMillis());
        MAX_PROCESSING_TIME = 2400000L; // 40 min
        SLEEP_TIME = 1800000; // 30 min
        try {
            testing = Boolean.parseBoolean(getPropertyValue("DEVELOPING_TESTING"));
        } catch (Exception ex) {
            testing = true;
        }
    }
    
    /**
     * Starts the worker thread
     */
    public void start() {
        stop.set(false);
        worker = new Thread(this);
        worker.start();
    }
    
    /**
     * interrupts the worker thread
     */
    public void interrupt() {
        running.set(false);
        worker.interrupt();
        serviceQueryThreadList.forEach(thread -> thread.interrupt());
    }
    
    /**
     * @return the stop
     */
    public boolean isStopped() {
        return stop.get();
    }
    
    /**
     * @return the running
     */
    public boolean isRunning() {
        return running.get();
    }
    
    /**
     * Checks if the worker thread is hung up
     * @return boolean, true is hung up, false otherwise
     */
    public boolean isHung() {
        return (System.currentTimeMillis() - lastProcessingTime.get() > MAX_PROCESSING_TIME);
    }
    
    /**
     * Runs worker thread.
     * Monitors Hosts
     */
    @Override
    public void run() {
        ServiceQueryThread serviceQueryThread;
        
        running.set(true);
        while(running.get()){
            try{
                lastProcessingTime.set(System.currentTimeMillis());
                
                /**
                 * Query Subscriptions
                 */
                if(running.get() && SERVICES != null){
                    for (String service : SERVICES){
                        if(!running.get()) {
                            break;
                        } else if (service.equalsIgnoreCase("Events") && testing) {
                        } else {
                            ServiceRegistrarClient.getInstance().query(service);
                        }
                    }
                }
                        
                // Sleep 10 seconds
                if(running.get()){
                    Thread.sleep(10000);
                }
                
                // Check statis of serviceQueryThreadList
                if(running.get()){
                    try {
                        if (SERVICES != null) {
                            if (serviceHostsMap == null) {
                                for (String service : SERVICES) {
                                    if(!running.get()) {
                                        break;
                                    } else if (service.equalsIgnoreCase("Events") && testing) {
                                    } else if(!serviceQueryThreadList.stream().anyMatch(thread -> thread.getService().equals(service))) {
                                        serviceQueryThreadList.add(new ServiceQueryThread(service));
                                    }
                                }
                            } else {
                                for (String service : SERVICES) {
                                    if(!running.get()) {
                                        break;
                                    } else if (service.equalsIgnoreCase("Events") && testing) {
                                    } else if((!serviceHostsMap.containsKey(service) || serviceHostsMap.get(service) == null || serviceHostsMap.get(service).isEmpty()) &&
                                            !serviceQueryThreadList.stream().anyMatch(thread -> thread.getService().equals(service))) {
                                        serviceQueryThreadList.add(new ServiceQueryThread(service));
                                    }
                                }
                            }
                        }
                        
                        for (Iterator<ServiceQueryThread> iterator = serviceQueryThreadList.iterator(); iterator.hasNext();) {
                            serviceQueryThread = iterator.next();
                            
                            if(!running.get()) {
                                break;
                            } else if (serviceQueryThread.isStopped() && serviceQueryThread.isDispose()) {
                                iterator.remove();
                            } else if (serviceQueryThread.isHung()) {
                                serviceQueryThread.interrupt();
                                Thread.sleep(10000);
                            }
                            
                            if(!running.get()) {
                                break;
                            } else if (serviceQueryThread.isStopped() && !serviceQueryThread.isDispose()) {
                                serviceQueryThread.start();
                            }
                        }
                    } catch(InterruptedException e) {
                        throw e;
                    } catch (Exception e) {
                        Logger.getLogger(SubscriptionMonitorThread.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    }
                }
                
                // Sleep
                if(running.get()){
                    Thread.sleep(SLEEP_TIME);
                }
            } catch(InterruptedException e){
            } catch(Exception e){
                Logger.getLogger(SubscriptionMonitorThread.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        Logger.getLogger(SubscriptionMonitorThread.class.getName()).log(Level.WARNING, "SubscriptionMonitorThread Stopped.");
        System.out.println("SubscriptionMonitorThread Stopped.");
        
        stop.set(true);
    }
}
