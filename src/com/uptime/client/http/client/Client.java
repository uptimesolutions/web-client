/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.client;

import com.uptime.client.http.utils.HttpDeleteWithBody;
import com.uptime.client.http.utils.HttpGetWithBody;
import com.uptime.client.utils.vo.HttpResponseVO;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class Client implements Serializable {
    final private int CONNECT_TIMEOUT;
    final private int REQUEST_TIMEOUT;

    /**
     * Constructor 
     */
    public Client() {
        CONNECT_TIMEOUT = 3000;
        REQUEST_TIMEOUT = 5000;
    }
    
    /**
     * Http get to service
     * @param uri, URI object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet (URI uri) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGet httpGet;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpGet = new HttpGet(uri);
            httpGet.setConfig(requestConfig);
            response = client.execute(httpGet);
            IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            responseVO.setEntity(writer.toString());
            
        } catch (HttpHostConnectException e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http get to service
     * @param uri, URI object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet_slf4j (URI uri) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGet httpGet;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpGet = new HttpGet(uri);
            httpGet.setConfig(requestConfig);
            response = client.execute(httpGet);
            IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            responseVO.setEntity(writer.toString());
            
        } catch (HttpHostConnectException e) {
            LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http get to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet (String service, String host, String requestPath) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGet httpGet;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "**Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpGet = new HttpGet(host + requestPath);
                httpGet.setConfig(requestConfig);
                response = client.execute(httpGet);
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                responseVO.setEntity(writer.toString());
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "***************Unable to connect to: {0}", service);
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "***************Exception when calling: {0}", service);
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http get to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet_slf4j (String service, String host, String requestPath) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGet httpGet;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("**Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpGet = new HttpGet(host + requestPath);
                httpGet.setConfig(requestConfig);
                response = client.execute(httpGet);
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                responseVO.setEntity(writer.toString());
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("***************Unable to connect to: " + service);
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("**************Exception when calling: " + service);
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http get to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object  
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGetWithBody httpGet;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpGet = new HttpGetWithBody(host + requestPath);
                if (entity instanceof String) {
                    httpGet.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpGet.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpGet.setConfig(requestConfig);
                response = client.execute(httpGet);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http get to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object  
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpGet_slf4j (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpGetWithBody httpGet;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpGet = new HttpGetWithBody(host + requestPath);
                if (entity instanceof String) {
                    httpGet.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpGet.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpGet.setConfig(requestConfig);
                response = client.execute(httpGet);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http post to service
     * @param uri, URI object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPost (URI uri, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPost httpPost;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();

            httpPost = new HttpPost(uri);
            if (entity instanceof String) {
                httpPost.setEntity(new StringEntity((String)entity));
            } else if (entity instanceof HttpEntity) {
                httpPost.setEntity((HttpEntity)entity);
            } else {
                throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
            }
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setConfig(requestConfig);
            response = client.execute(httpPost);

            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }

        } catch (HttpHostConnectException e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http post to service
     * @param uri, URI object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPost_slf4j (URI uri, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPost httpPost;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();

            httpPost = new HttpPost(uri);
            if (entity instanceof String) {
                httpPost.setEntity(new StringEntity((String)entity));
            } else if (entity instanceof HttpEntity) {
                httpPost.setEntity((HttpEntity)entity);
            } else {
                throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
            }
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setConfig(requestConfig);
            response = client.execute(httpPost);

            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }

        } catch (HttpHostConnectException e) {
            LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http post to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPost (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPost httpPost;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpPost = new HttpPost(host + requestPath);
                if (entity instanceof String) {
                    httpPost.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpPost.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setConfig(requestConfig);
                response = client.execute(httpPost);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http post to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPost_slf4j (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPost httpPost;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpPost = new HttpPost(host + requestPath);
                if (entity instanceof String) {
                    httpPost.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpPost.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setConfig(requestConfig);
                response = client.execute(httpPost);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http put to service
     * @param uri, URI object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPut (URI uri, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPut httpPut;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpPut = new HttpPut(uri);
            if (entity instanceof String) {
                httpPut.setEntity(new StringEntity((String)entity));
            } else if (entity instanceof HttpEntity) {
                httpPut.setEntity((HttpEntity)entity);
            } else {
                throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
            }
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");
            httpPut.setConfig(requestConfig);
            response = client.execute(httpPut);
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }
            
        } catch (HttpHostConnectException e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http put to service
     * @param uri, URI object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPut_slf4j (URI uri, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPut httpPut;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpPut = new HttpPut(uri);
            if (entity instanceof String) {
                httpPut.setEntity(new StringEntity((String)entity));
            } else if (entity instanceof HttpEntity) {
                httpPut.setEntity((HttpEntity)entity);
            } else {
                throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
            }
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");
            httpPut.setConfig(requestConfig);
            response = client.execute(httpPut);
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }
            
        } catch (HttpHostConnectException e) {
            LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http put to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPut (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPut httpPut;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpPut = new HttpPut(host + requestPath);
                if (entity instanceof String) {
                    httpPut.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpPut.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpPut.setHeader("Accept", "application/json");
                httpPut.setHeader("Content-type", "application/json");
                httpPut.setConfig(requestConfig);
                response = client.execute(httpPut);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http put to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpPut_slf4j (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpPut httpPut;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpPut = new HttpPut(host + requestPath);
                if (entity instanceof String) {
                    httpPut.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpPut.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpPut.setHeader("Accept", "application/json");
                httpPut.setHeader("Content-type", "application/json");
                httpPut.setConfig(requestConfig);
                response = client.execute(httpPut);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param uri, URI object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete (URI uri) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDelete httpDelete;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpDelete = new HttpDelete(uri);
            httpDelete.setConfig(requestConfig);
            response = client.execute(httpDelete);
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }
            
        } catch (HttpHostConnectException e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param uri, URI object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete_slf4j (URI uri) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDelete httpDelete;
        
        try(CloseableHttpClient client = HttpClients.createDefault()){
            requestConfig = RequestConfig.custom()
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                .build();
                
            httpDelete = new HttpDelete(uri);
            httpDelete.setConfig(requestConfig);
            response = client.execute(httpDelete);
            
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(true);
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            try {
                IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                responseVO.setEntity(writer.toString());
            } catch (Exception e) {
                responseVO.setEntity(null);
            }
            
        } catch (HttpHostConnectException e) {
            LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
            responseVO = null;
        } catch (Exception e) {
            LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
            responseVO = null;
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete (String service, String host, String requestPath) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDelete httpDelete;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpDelete = new HttpDelete(host + requestPath);
                httpDelete.setConfig(requestConfig);
                response = client.execute(httpDelete);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete_slf4j (String service, String host, String requestPath) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDelete httpDelete;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpDelete = new HttpDelete(host + requestPath);
                httpDelete.setConfig(requestConfig);
                response = client.execute(httpDelete);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDeleteWithBody httpDelete;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Issue with obtaining a host from the service: {0}", service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpDelete = new HttpDeleteWithBody(host + requestPath);
                if (entity instanceof String) {
                    httpDelete.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpDelete.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpDelete.setConfig(requestConfig);
                response = client.execute(httpDelete);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "HttpHostConnectException: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception: {0}", e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
    
    /**
     * Http delete to service
     * @param service, String object of the service name
     * @param host, String object 
     * @param requestPath, String object
     * @param entity, Object, Must be instance of String Object or HttpEntity Object.
     * @return HttpResponseVO object
     */
    public HttpResponseVO httpDelete_slf4j (String service, String host, String requestPath, Object entity) {
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        RequestConfig requestConfig;
        StringWriter writer;
        HttpDeleteWithBody httpDelete;
        
        if (host == null || host.startsWith("No Host available")){
            try {
                LoggerFactory.getLogger(Client.class).warn("Issue with obtaining a host from the service: " + service);
            } catch (Exception e) {}
            responseVO = new HttpResponseVO();
            responseVO.setHostFound(false);
        } else {
            try(CloseableHttpClient client = HttpClients.createDefault()){
                requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECT_TIMEOUT)
                    .setConnectionRequestTimeout(REQUEST_TIMEOUT)
                    .build();
                
                httpDelete = new HttpDeleteWithBody(host + requestPath);
                if (entity instanceof String) {
                    httpDelete.setEntity(new StringEntity((String)entity));
                } else if (entity instanceof HttpEntity) {
                    httpDelete.setEntity((HttpEntity)entity);
                } else {
                    throw new Exception("Error: Invalid entity type given. Must be instance of String Object or HttpEntity Object."); 
                }
                httpDelete.setConfig(requestConfig);
                response = client.execute(httpDelete);
                
                responseVO = new HttpResponseVO();
                responseVO.setHostFound(true);
                responseVO.setStatusCode(response.getStatusLine().getStatusCode());
                try {
                    IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
                    responseVO.setEntity(writer.toString());
                } catch (Exception e) {
                    responseVO.setEntity(null);
                }
                
            } catch (HttpHostConnectException e) {
                LoggerFactory.getLogger(Client.class).warn("HttpHostConnectException: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            } catch (Exception e) {
                LoggerFactory.getLogger(Client.class).warn("Exception: " + e.getMessage());
                ServiceRegistrarClient.getInstance().publishCircuitBreaker(host, service);
                responseVO = null;
            }
        }
        return responseVO;
    }
}
