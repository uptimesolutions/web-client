/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.client;

import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import com.uptime.client.http.utils.JSONParser;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.client.utils.vo.ServiceHostVO;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.apache.http.client.entity.EntityBuilder;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class ServiceRegistrarClient extends Client implements Serializable {
    public volatile static Map<String,ArrayList<ServiceHostVO>> serviceHostsMap;
    public volatile static Map<String,Integer> serviceHostIndexMap;
    private final List<String> peerRegistrarUrlList;
    private final AtomicInteger peerRegistrarUrlIndex;
    public static Semaphore mutex = new Semaphore(1);
    public static boolean running = true;
    private static ServiceRegistrarClient instance = null;
    private final JSONParser parser;
    private final String PROTOCOL;
    
    /**
     * Constructor
     */
    private ServiceRegistrarClient() {
        String registrars;
        
        parser = new JSONParser();
        peerRegistrarUrlList = new ArrayList();
        if ((registrars = getPropertyValue("REGISTRAR_URL")) != null) {
            for (String registrar : registrars.split(",")) {
                if (registrar != null && !registrar.isEmpty()) {
                    peerRegistrarUrlList.add(registrar);
                }
            }
                    
        }
        peerRegistrarUrlIndex = new AtomicInteger(0);
        PROTOCOL = getPropertyValue("TOMCAT_PROTOC0L");
    }
    
    /**
     * Constructor
     * @param registrars, String Object, registrars can represent one registrar or multiple separated be a commas (exp - http://10.1.10.25:8081,http://10.1.10.26:8081)
     * @param protocol, String Object
     */
    private ServiceRegistrarClient(String registrars, String protocol) {
        parser = new JSONParser();
        peerRegistrarUrlList = new ArrayList();
        if (registrars != null) {
            for (String registrar : registrars.split(",")) {
                if (registrar != null && !registrar.isEmpty()) {
                    peerRegistrarUrlList.add(registrar);
                }
            }
                    
        }
        peerRegistrarUrlIndex = new AtomicInteger(0);
        PROTOCOL = protocol;
    }  
    
    /**
     * Constructor
     * @param registrars, Array Object of Strings
     * @param protocol, String Object
     */
    private ServiceRegistrarClient(String[] registrars, String protocol) {
        parser = new JSONParser();
        peerRegistrarUrlList = new ArrayList();
        if (registrars != null) {
            for (String registrar : registrars) {
                if (registrar != null && !registrar.isEmpty()) {
                    peerRegistrarUrlList.add(registrar);
                }
            }
                    
        }
        peerRegistrarUrlIndex = new AtomicInteger(0);
        PROTOCOL = protocol;
    } 

    /**
     * Return an instance of the class
     * @return ServiceRegistrarClient Object
     */
    public static ServiceRegistrarClient getInstance(){
        if(instance == null) {
            instance = new ServiceRegistrarClient();
        }
        return instance;
    }
    
    /**
     * Return an instance of the class
     * @param registrars, String Object, registrars can represent one registrar or multiple separated be a commas (exp - http://10.1.10.25:8081,http://10.1.10.26:8081)
     * @param protocol the protocol
     * @return ServiceRegistrarClient Object
     */
    public static ServiceRegistrarClient getInstance(String registrars, String protocol){
        if(instance == null) {
            instance = new ServiceRegistrarClient(registrars,protocol);
        }
        return instance;
    }
    
    /**
     * Return an instance of the class
     * @param registrars, Array Object of Strings
     * @param protocol the protocol
     * @return ServiceRegistrarClient Object
     */
    public static ServiceRegistrarClient getInstance(String[] registrars, String protocol){
        if(instance == null) {
            instance = new ServiceRegistrarClient(registrars,protocol);
        }
        return instance;
    }

    /**
     * Subscribes to the serviceRegistrar
     * @param names, Array of String objects of the services being subscribed too
     * @param callbackUrl, String Object, Url of the web application
     */
    public void subscribe(String[] names, String callbackUrl) {
        Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "Subscribe to registrar");
        HttpResponseVO responseVO;
        EntityBuilder builder;
        StringBuilder json;
        
        try {
            // creates a json for subscribing
            json = new StringBuilder();
            json.append("{\"names\":[");
            for(int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                .append("],\"ipAddress\":\"\",")
                .append("\"port\":\"0\",")
                .append("\"url\":\"").append(callbackUrl).append("\",")
                .append("\"source\":\"gui\"}");
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "Json : {0}", json.toString());
            
            // Subscribing to serviceRegistrar
            builder = EntityBuilder.create();
            builder.setText(json.toString());
            
            if ((responseVO = httpPost(new URI(getPeerRegistrarURL() + "/subscribe"), builder.build())) != null) {
                Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "subscribe Resp code {0}", responseVO.getStatusCode());
            } else {
                throw new Exception("ResponseVO is Null");
            }
            
        }catch(Exception e){
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "*************After Failed subscribe*************");
        }
    }

    /**
     * Subscribes to the serviceRegistrar
     * @param names, Array of String objects of the services being subscribed too
     * @param callbackUrl, String Object, Url of the web application
     */
    public void subscribe_slf4j(String[] names, String callbackUrl) {
        LoggerFactory.getLogger(ServiceRegistrarClient.class).info("Subscribe to registrar");
        HttpResponseVO responseVO;
        EntityBuilder builder;
        StringBuilder json;
        
        try {
            // creates a json for subscribing
            json = new StringBuilder();
            json.append("{\"names\":[");
            for(int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                .append("],\"ipAddress\":\"\",")
                .append("\"port\":\"0\",")
                .append("\"url\":\"").append(callbackUrl).append("\",")
                .append("\"source\":\"gui\"}");
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("Json : {}", json.toString());
            
            // Subscribing to serviceRegistrar
            builder = EntityBuilder.create();
            builder.setText(json.toString());
            
            if ((responseVO = httpPost_slf4j(new URI(getPeerRegistrarURL() + "/subscribe"), builder.build())) != null) {
                LoggerFactory.getLogger(ServiceRegistrarClient.class).info("subscribe Resp code {}", responseVO.getStatusCode());
            } else {
                throw new Exception("ResponseVO is Null");
            }
            
        }catch(Exception e){
            LoggerFactory.getLogger(ServiceRegistrarClient.class).error(e.getMessage(), e);
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("*************After Failed subscribe*************");
        }
    }
    
    /**
     * Unsubscribes from the serviceRegistrar
     * @param names, Array of String objects of the services being subscribed too
     * @param callbackUrl, String Object, Url of the web application
     */
    public void unsubscribe(String[] names, String callbackUrl) {
        Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "Unsubscribe to registrar");
        HttpResponseVO responseVO;
        EntityBuilder builder;
        StringBuilder json;
        
        try {
            // creates a json for unsubscribing
            json = new StringBuilder();
            json.append("{\"names\":[");
            for(int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                .append("],\"ipAddress\":\"\",")
                .append("\"port\":\"0\",")
                .append("\"url\":\"").append(callbackUrl).append("\",")
                .append("\"source\":\"gui\"}");
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "Json : {0}", json.toString());
            
            // Unsubscribing to serviceRegistrar
            builder = EntityBuilder.create();
            builder.setText(json.toString());
            
            if ((responseVO = httpPut(new URI(getPeerRegistrarURL() + "/subscribe"), builder.build())) != null) {
                Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "unsubscribe Resp code {0}", responseVO.getStatusCode());
            } else {
                throw new Exception("ResponseVO is Null");
            }
        } catch (Exception e) {
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "*************After Failed unsubscribe*************");
        }  
    }
    
    /**
     * Unsubscribes from the serviceRegistrar
     * @param names, Array of String objects of the services being subscribed too
     * @param callbackUrl, String Object, Url of the web application
     */
    public void unsubscribe_slf4j(String[] names, String callbackUrl) {
        LoggerFactory.getLogger(ServiceRegistrarClient.class).info("Unsubscribe to registrar");
        HttpResponseVO responseVO;
        EntityBuilder builder;
        StringBuilder json;
        
        try {
            // creates a json for unsubscribing
            json = new StringBuilder();
            json.append("{\"names\":[");
            for(int i = 0; i < names.length; i++) {
                json.append("{\"name\":\"").append(names[i]).append(i < names.length - 1 ? "\"}," : "\"}");
            }
            json
                .append("],\"ipAddress\":\"\",")
                .append("\"port\":\"0\",")
                .append("\"url\":\"").append(callbackUrl).append("\",")
                .append("\"source\":\"gui\"}");
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("Json : {}", json.toString());
            
            // Unsubscribing to serviceRegistrar
            builder = EntityBuilder.create();
            builder.setText(json.toString());
            
            if ((responseVO = httpPut_slf4j(new URI(getPeerRegistrarURL() + "/subscribe"), builder.build())) != null) {
                LoggerFactory.getLogger(ServiceRegistrarClient.class).info("unsubscribe Resp code {}", responseVO.getStatusCode());
            } else {
                throw new Exception("ResponseVO is Null");
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(ServiceRegistrarClient.class).error(e.getMessage(), e);
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("*************After Failed unsubscribe*************");
        }  
    }
    
    /**
     * Query the serviceRegistrar for the given service
     * @param service, String Object
     */
    public void query(String service) {
        HttpResponseVO responseVO;
        
        try {
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "Querying registrar for the Service -{0}", service);
            if ((responseVO = httpGet(new URI(getPeerRegistrarURL() + "/query?name=" + service))) != null) {
                if (responseVO.getStatusCode() == 200) {
                    parser.parseQuery(responseVO.getEntity());
                } else {
                    Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "query Resp code {0}", responseVO.getStatusCode());
                }
            } else {
                throw new Exception("ResponseVO is Null");
            }
        } catch (Exception e) {
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "*************After Failed query*************");
        }  
    }
    
    /**
     * Query the serviceRegistrar for the given service
     * @param service, String Object
     */
    public void query_slf4j(String service) {
        HttpResponseVO responseVO;
        
        try {
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("Querying registrar for the Service -" + service);
            if ((responseVO = httpGet_slf4j(new URI(getPeerRegistrarURL() + "/query?name=" + service))) != null) {
                if (responseVO.getStatusCode() == 200) {
                    parser.parseQuery(responseVO.getEntity());
                } else {
                    LoggerFactory.getLogger(ServiceRegistrarClient.class).info("query Resp code {}", responseVO.getStatusCode());
                }
            } else {
                throw new Exception("ResponseVO is Null");
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(ServiceRegistrarClient.class).error(e.getMessage(), e);
            LoggerFactory.getLogger(ServiceRegistrarClient.class).info("*************After Failed query*************");
        }  
    }
    
    /**
     * Creates and returns a host String for the given service name
     * @param serviceName, String object
     * @return String object
     */
    public String getServiceHostURL(String serviceName) {
        ArrayList<ServiceHostVO> serviceHostList;
        ServiceHostVO serviceHostVO;
        String url;
        int index;
        
        url = "No Host available for " + serviceName;
        if(serviceHostsMap != null && !serviceHostsMap.isEmpty() && serviceHostIndexMap != null && !serviceHostIndexMap.isEmpty()) {
            serviceHostList = null;
            index = 0;
            try {
                mutex.acquire();
                if (serviceHostsMap.containsKey(serviceName)) {
                    serviceHostList = serviceHostsMap.get(serviceName);
                }
                if (serviceHostIndexMap.containsKey(serviceName)) {
                    index =  serviceHostIndexMap.get(serviceName);
                }
                if (serviceHostList != null && !serviceHostList.isEmpty()) {
                    serviceHostVO = serviceHostList.get(index);
                    url = PROTOCOL + "://" + serviceHostVO.getIp() + ":" + serviceHostVO.getPort();
                    index++;
                    index = index == serviceHostList.size() ? 0 : index;
                    serviceHostIndexMap.put(serviceName, index);
                }
               mutex.release();
            } catch(Exception e){
                Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        //Logger.getLogger(ServiceRegistrarClient.class.getName()).log(Level.INFO, "url in getServiceHostURL : {0}", url);
        return url;
    }
    
    /**
     * returns a peer registrar url based on its index
     * @return String object
     */
    private String getPeerRegistrarURL() {
        String url = "";
        
        if (!peerRegistrarUrlList.isEmpty()) {
            if (peerRegistrarUrlIndex.get() >= peerRegistrarUrlList.size()) {
                peerRegistrarUrlIndex.set(0);
            }
            url = peerRegistrarUrlList.get(peerRegistrarUrlIndex.get());
            peerRegistrarUrlIndex.incrementAndGet();
        }
        return url;
    }
    
    /**
     * Creates and returns a host String for the given service name
     * @param serviceName, String object
     * @return String object
     */
    public String getServiceHostURL_slf4j(String serviceName) {
        ArrayList<ServiceHostVO> serviceHostList;
        ServiceHostVO serviceHostVO;
        String url;
        int index;
        
        url = "No Host available for " + serviceName;
        if(serviceHostsMap != null && !serviceHostsMap.isEmpty() && serviceHostIndexMap != null && !serviceHostIndexMap.isEmpty()) {
            serviceHostList = null;
            index = 0;
            try {
                mutex.acquire();
                if (serviceHostsMap.containsKey(serviceName)) {
                    serviceHostList = serviceHostsMap.get(serviceName);
                }
                if (serviceHostIndexMap.containsKey(serviceName)) {
                    index =  serviceHostIndexMap.get(serviceName);
                }
                if (serviceHostList != null && !serviceHostList.isEmpty()) {
                    serviceHostVO = serviceHostList.get(index);
                    url = "http" + "://" + serviceHostVO.getIp() + ":" + serviceHostVO.getPort();
                    index++;
                    index = index == serviceHostList.size() ? 0 : index;
                    serviceHostIndexMap.put(serviceName, index);
                }
               mutex.release();
            } catch(Exception e){
                LoggerFactory.getLogger(ServiceRegistrarClient.class).error(e.getMessage(), e);
            }
        }
        //LoggerFactory.getLogger(ServiceRegistrarClient.class).info("url in getServiceHostURL : {}", url);
        return url;
    }
    
    /**
     * Informs the ServiceRegistrar of a CircuitBreaker.
     * @param host, String object
     * @param name, String object
     */
    public void publishCircuitBreaker (String host, String name) {
        Pattern pattern = Pattern.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        StringBuilder json = new StringBuilder();
        String[] elements;
        int port;
        
        try {
            if(!host.equalsIgnoreCase(getPeerRegistrarURL()) && (elements = host.replace(PROTOCOL + "://", "").split(":")) != null && elements.length == 2) {
                if (!pattern.matcher(elements[0]).matches() && (port = Integer.parseInt(elements[1])) > -1 && port <= 65535) {
                    json
                        .append("{\"name\":\"").append(name).append("\",")
                        .append("\"ipAddress\":\"").append(elements[0]).append("\",")
                        .append("\"port\":\"").append(elements[1]).append("\"}");

                    httpPost("ServiceRegistrar", getPeerRegistrarURL(), "/circuitbreaker", json.toString());
                }
            }
        } catch (Exception ex) {}
    }
}
