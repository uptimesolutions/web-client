/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.client;

import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class EventsClient extends Client implements Serializable {
    private static EventsClient instance = null;
    private final String PORT;
    private boolean testing;
    
    /**
     * Constructor
     */
    private EventsClient() {
        PORT = getPropertyValue("TOMCAT_PORT");
        try {
            testing = Boolean.parseBoolean(getPropertyValue("DEVELOPING_TESTING"));
        } catch (Exception ex) {
            testing = true;
        }
    }
    
    private EventsClient(String port, boolean testing) {
        PORT = port;
        this.testing = testing;
    }

    /**
     * Return an instance of the class
     * @return EventsClient Object
     */
    public static EventsClient getInstance(){
        if(instance == null) {
            instance = new EventsClient();
        }
        return instance;
    }
    
    /**
     * For clients that do not use the specific properties TOMCAT_PORT and DEVELOPING_TESTING
     * @param port
     * @param testing
     * @return 
     */
    public static EventsClient getInstance(String port, boolean testing){
        if(instance == null) {
            instance = new EventsClient(port,testing);
        }
        return instance;
    }

    /**
     * Attempt to send an event to the Events service
     * @param host, the IP address of the Event Service instance
     * @param application, the name of the application sending the event
     * @param stackTrace, the data / details of the event
     */
    public void sendEvent(String host, String application, StackTraceElement[] stackTrace) {
        String ipAddress = null;
        StringBuilder json;
                
        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            Logger.getLogger(EventsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        try {
            if (!testing && stackTrace != null) {
                json = new StringBuilder();
                json
                    .append("{\"application\":\"").append(application).append("\",")
                    .append("\"created_date\":\"").append(new Date().getTime()).append("\",")
                    .append("\"ip_address\":\"").append(ipAddress).append("\",")
                    .append("\"port\":\"").append(PORT).append("\",")
                    .append("\"data\":\"");
                for (StackTraceElement ele : stackTrace) {
                    json.append(ele.toString()).append("<br />");
                }
                json.append("\"}");

                httpPost("Events", host, "/events", json.toString());
            }
        }catch (Exception e) {
            Logger.getLogger(EventsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Attempt to send an event to the Events service
     * @param host the IP address of the Event Service instance
     * @param application the name of the application sending the event
     * @param data the data / details of the event
     */
    public void sendEvent(String host, String application, String data) {
        String ipAddress = null;
        StringBuilder json;
                
        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            Logger.getLogger(EventsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        try {
            if (!testing && data != null) {
                json = new StringBuilder();
                json
                    .append("{\"application\":\"").append(application).append("\",")
                    .append("\"created_date\":\"").append(new Date().getTime()).append("\",")
                    .append("\"ip_address\":\"").append(ipAddress).append("\",")
                    .append("\"port\":\"").append(PORT).append("\",")
                    .append("\"data\":\"").append(data).append("\"}");
                httpPost("Events", host, "/events", json.toString());
            }
        }catch (Exception e) {
            Logger.getLogger(EventsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
