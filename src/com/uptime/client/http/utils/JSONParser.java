/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.ServiceHostVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class JSONParser {
    
    /**
     * Parses the query json from the serviceRegistrar
     * @param content
     * @throws Exception 
     */
    public void parseQuery(String content) throws Exception {
        ArrayList<ServiceHostVO> serviceHostVOList;
        ServiceHostVO serviceHostVO;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray array;
        String name;
        
        if(ServiceRegistrarClient.serviceHostsMap == null) {
            ServiceRegistrarClient.serviceHostsMap = new HashMap();
        }
        if(ServiceRegistrarClient.serviceHostIndexMap == null) {
            ServiceRegistrarClient.serviceHostIndexMap = new HashMap();
        }

        if ((element  = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null && jsonObject.has("name") && jsonObject.has("hosts")) {
            Logger.getLogger(JSONParser.class.getName()).log(Level.INFO, "***JSON Content***: {0}", jsonObject);
            name = jsonObject.get("name").toString().replaceAll("^\"|\"$", "");

            if ((array = jsonObject.getAsJsonArray("hosts")) != null && !array.isEmpty()) {
                serviceHostVOList = new ArrayList();
                for (int i = 0; i < array.size(); i++) {
                    serviceHostVO = new ServiceHostVO();
                    if ((jsonObject = array.get(i).getAsJsonObject()) != null) {
                        if (jsonObject.has("ipAddress")) {
                            try {
                                serviceHostVO.setIp(jsonObject.get("ipAddress").getAsString().replace("[", "").replace("]", ""));
                            }catch (Exception ex) {}
                        }
                        if (jsonObject.has("port")) {
                            try {
                                serviceHostVO.setPort(jsonObject.get("port").getAsInt());
                            }catch (Exception ex) {}
                        }
                        if (jsonObject.has("circuitBreaker")) {
                            try {
                                serviceHostVO.setCircuitBreaker(jsonObject.get("circuitBreaker").getAsBoolean());
                            }catch (Exception ex) {}
                        }
                    }
                    serviceHostVOList.add(serviceHostVO);
                }

                try {
                    ServiceRegistrarClient.mutex.acquire();
                    ServiceRegistrarClient.serviceHostsMap.put(name, serviceHostVOList);
                    ServiceRegistrarClient.serviceHostIndexMap.put(name, 0);
                } finally {
                    ServiceRegistrarClient.mutex.release();  
                }
            } else {
                Logger.getLogger(JSONParser.class.getName()).log(Level.INFO,"No hosts received from json.");
            }
        }
    }
}
