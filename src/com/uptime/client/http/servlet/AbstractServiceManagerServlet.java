/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.client.http.servlet;

import com.uptime.client.http.client.EventsClient;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.http.thread.SubscriptionMonitorThread;
import com.uptime.client.http.utils.JSONParser;
import com.uptime.client.utils.vo.ServiceHostVO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author twilcox
 */
public abstract class AbstractServiceManagerServlet extends HttpServlet implements Serializable {
    /***********************************TO DO*************************************
        The following variables need to be added to the web.xml:
            "properties-file" - The required properties file
                 (Note: the file needs to be placed in the directory WEB-INF/classes)
            "callback-endpoint" - The end point of the callback for this Servlet
            "required-services" - The required services needed for this application 
                 (Note: the service names should only be separated by a comma)
            "application-name" - The name of this application
    
        Example:
            <servlet>
                <servlet-name>ServiceManagerServlet</servlet-name>
                <servlet-class>com.uptime.worldview.http.servlet.ServiceManagerServlet</servlet-class>
                <init-param>
                    <param-name>required-services</param-name>
                    <param-value>Playlist,Events</param-value>
                </init-param>
                <init-param>
                    <param-name>properties-file</param-name>
                    <param-value>application.properties</param-value>
                </init-param>
                <init-param>
                    <param-name>callback-endpoint</param-name>
                    <param-value>/serviceManagerServlet</param-value>
                </init-param>
                <init-param>
                    <param-name>application-name</param-name>
                    <param-value>Worldview Cloud</param-value>
                </init-param>
                <load-on-startup>1</load-on-startup>
            </servlet>
            <servlet-mapping>
                <servlet-name>ServiceManagerServlet</servlet-name>
                <url-pattern>/serviceManagerServlet</url-pattern>
            </servlet-mapping>
    *****************************************************************************/
    private static Properties properties = null;
    private static String propertyEnvironment = null;
    
    private SubscriptionMonitorThread subscriptionMonitorThread;
    private String[] requiredServices;
    private String callbackUrl, applicationName;
    private final JSONParser parser;

    /**
     * Constructor
     */
    public AbstractServiceManagerServlet() {
        subscriptionMonitorThread = null;
        parser = new JSONParser();
    }
    
    /**
     * Post Constructor
     * @param servletConfig, ServletConfig Object
     * @throws ServletException 
     */
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        StringBuilder url;
        
        try {
            
            // Get Property file location from the web.xml.
            try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(servletConfig.getInitParameter("properties-file"))) {
                properties = new Properties();
                properties.load(is);
                if((propertyEnvironment = properties.getProperty("env")) == null || propertyEnvironment.isEmpty()) {
                    throw new Exception("Env variable missing");
                } 
                    
            } catch(Exception e) {
                Logger.getLogger(AbstractServiceManagerServlet.class.getName()).log(Level.SEVERE, "Fail to load required properties.");
                properties = null;
                propertyEnvironment = null;
                throw e;
            } 
            
            // Construct the callbackUrl
            url = new StringBuilder();
            url
                .append(getPropertyValue("TOMCAT_PROTOC0L")).append("://")
                .append(InetAddress.getLocalHost().getHostAddress())
                .append(":")
                .append(getPropertyValue("TOMCAT_PORT"))
                .append(servletConfig.getInitParameter("callback-endpoint"));
            callbackUrl = url.toString();
            
            // Get Required Services from the web.xml.
            // Service names should be a comma separated list
            requiredServices = servletConfig.getInitParameter("required-services").split(",");
            
            // Get Application name from the web.xml
            applicationName = servletConfig.getInitParameter("application-name"); 
            
        } catch (Exception ex) {
            Logger.getLogger(AbstractServiceManagerServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            callbackUrl = null;
            requiredServices = null;
        }
        
        // Subscribe to the serviceRegistrar
        if (callbackUrl != null && requiredServices != null && requiredServices.length > 0) {
            ServiceRegistrarClient.getInstance().subscribe(requiredServices, callbackUrl);  
            subscriptionMonitorThread = new SubscriptionMonitorThread(requiredServices);
            subscriptionMonitorThread.start();
        }
    }

   
    /**
     * HTTP POST handler
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        Logger.getLogger(AbstractServiceManagerServlet.class.getName()).log(Level.INFO,"****doPost Called ****");
        StringBuilder content;
        String line;
        
        try (BufferedReader reader = request.getReader()){
            content = new StringBuilder();
            
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
            
            parser.parseQuery(content.toString());
        } catch (Exception e) {
            Logger.getLogger(AbstractServiceManagerServlet.class.getName()).log(Level.SEVERE, "Exception in doPost: {0}", e.toString());
            EventsClient.getInstance().sendEvent(ServiceRegistrarClient.getInstance().getServiceHostURL("Events"), applicationName, e.getStackTrace());
        }
    }
    
   
    /**
     * HTTP GET handler
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder sb = new StringBuilder();
        List<ServiceHostVO> hosts;
        List<String> keys;
        ServiceHostVO host;
        
        
        if(ServiceRegistrarClient.serviceHostsMap != null && !ServiceRegistrarClient.serviceHostsMap.isEmpty()){
            keys = new ArrayList(ServiceRegistrarClient.serviceHostsMap.keySet());
            for(int i = 0; i < keys.size(); i++) {
                sb.append("{\"name\":\"").append(keys.get(i)).append("\",");
                sb.append("\"hosts\":[");
                
                hosts = ServiceRegistrarClient.serviceHostsMap.get(keys.get(i));
                for(int j = 0; j < hosts.size(); j++) {
                    host = hosts.get(j);
                    sb.append("{\"ipAddress\":\"").append(host.getIp()).append("\",");
                    sb.append("\"port\":").append(host.getPort()).append(",");
                    sb.append("\"circuitBreaker\":").append(host.isCircuitBreaker()).append(j < hosts.size() - 1 ? "}," : "}]}");
                }
                sb.append(i < keys.size() - 1 ? "," : "");
            }
        } else {
            sb.append("{\"outcome\":\"serviceHostsMap is null or empty.\"}");
        }
        
        // Set response content type
        response.setContentType("application/json");
        response.setStatus(200);
        response.getWriter().write(sb.toString());
    }

    /**
     * Unsubscribe the application from the serviceRegistrar
     * And Stop the SubscriptionMonitorThread and all sub threads
     */
    @Override
    public void destroy(){
        if (callbackUrl != null && requiredServices != null && requiredServices.length > 0) {
            ServiceRegistrarClient.getInstance().unsubscribe(requiredServices, callbackUrl); 
            if (subscriptionMonitorThread != null) {
                subscriptionMonitorThread.interrupt();
            }
        }
    }
    
    /**
     * Return the the value for the given property
     * @param property, String Object
     * @return, String Object
     */
    public static String getPropertyValue(String property) {
        if(properties != null && propertyEnvironment != null && property != null && !property.isEmpty()) {
            return properties.getProperty(property + propertyEnvironment);
        }
        return null;
    }

    public static String getPropertyEnvironment() {
        return propertyEnvironment;
    }
    
    /**
     * Return a List of property keys
     * @return, List Object of String Objects
     */
    public static List<String> getListOfPropertyKeys() {
        List<String> list = new ArrayList();
        if(properties  != null && propertyEnvironment != null) {
            properties.keySet().stream().filter(p -> !p.equals(propertyEnvironment)).sorted().forEachOrdered(p -> {
                String tmp = (String)p;
                tmp = tmp.replace(propertyEnvironment, "");
                list.add(tmp);
            });
        }
        return list;
    } 
    
    public abstract void otherRoutineActions();
}
